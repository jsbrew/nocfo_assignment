import { useRef } from 'react';
import * as Icons from '@storybook/icons';
import styles from './Card.module.css';

/*===== Component with props from parent =====*/
function Card({ type, heading, description, date, priority }) {

    /*===== Variables =====*/
    const cardBodyRef = useRef(null);
    const checkboxRef = useRef(null);
    const chevronRef = useRef(null);

    /*===== Function: toggle card body based on checkbox status (checked or not) =====*/
    function toggleCollapse() {

        /*===== Handling an error when useRef has a minor change to return null (Really rare case tho...) =====*/
        if (!cardBodyRef.current || !checkboxRef.current || !chevronRef.current) {
            console.error('cardBodyRef or checkboxRef is null');
            return;
        }

        /*===== If checkbox is checked, add or remove class + rotate chevron icon =====*/
        if (checkboxRef.current.checked) {
            cardBodyRef.current.classList.add(`${styles.cardOpen}`);
            chevronRef.current.style.transform = 'rotate(180deg)';
        }
        else {
            cardBodyRef.current.classList.remove(`${styles.cardOpen}`);
            chevronRef.current.style.transform = 'rotate(0)';
        }
    }

    /*===== Render Card =====*/
    return (
        <div className={`${styles.card}`}>
            <label className={`${styles.card__heading} ${type ? styles.mainPoint : ''}`} data-date={date}>
                <h6>
                    {/*===== Card heading with underscores replaced =====*/}
                    {heading ? heading.replace(/_/g, ' ') : type ? type.replace(/_/g, ' ') : "TRANSACTION"}
                </h6>
                <div className={`${styles.card__indicator}`}>
                    <Icons.ChevronSmallUpIcon ref={chevronRef} />
                </div>
                <input type="checkbox" onChange={toggleCollapse} ref={checkboxRef} />
            </label>
            <div className={`${styles.card__body}`} ref={cardBodyRef}>
                <p>
                    {description ? description : "---"}
                </p>
            </div>
            <div className={`${styles.card__footer}`}>
                <small className={`${styles.card__date}`}>
                    <Icons.CalendarIcon />
                    {date ? date : "yyyy-mm-dd"}
                </small>
                <small className={`${styles.card__priority} ${styles[priority]}`}>
                    {priority ? priority : "-"}
                </small>
            </div>
        </div>
    )
}
export default Card;