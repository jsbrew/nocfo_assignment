import React, { useRef } from 'react';
import Card from './Card';
import styles from './Timeline.module.css';
import * as Icons from '@storybook/icons';

/*===== Component with data prop  =====*/
function Timeline({ items, title }) {
    /*===== Variables  =====*/
    const scrollContainerRef = useRef(null);
    const cardRef = useRef(null);

    /*===== Function: scroll right with scroll-snap  =====*/
    function scrollRight() {
        if (scrollContainerRef.current && cardRef.current) {
            const cardWidth = cardRef.current.offsetWidth + 16; /*===== + number should be the gap width between card elements  =====*/
            scrollContainerRef.current.scrollLeft += cardWidth;
        }
    }
    
    /*===== Function: scroll left with scroll-snap  =====*/
    function scrollLeft() {
            if (scrollContainerRef.current && cardRef.current) {
                const cardWidth = cardRef.current.offsetWidth + 16; /*===== + number should be the gap width between card elements  =====*/
                scrollContainerRef.current.scrollLeft -= cardWidth;
            }
    }
    
    /*===== Render Card =====*/
    return (
        <div className={`${styles.timelineContainer}`}>
            <div className={`${styles.timelineHeading}`}>
                <h4>{title}</h4>
            </div>
            <div className={`${styles.timeline}`}>
                <ul className={`${styles.timeline__items}`} ref={scrollContainerRef}>
                    {/*===== Map through items and create a card for each data item  =====*/}
                    {items.map((item, index) => (
                        <li key={index} className={`${styles.timeline__item}`} ref={cardRef}>
                            <Card
                                type={item.type}
                                heading={item.label}
                                description={item.description}
                                date={item.date}
                                priority={item.priority}
                            />
                        </li>
                    ))}
                </ul>

                {/*===== Scroll-snap buttons. Maybe later a button to open menu, where could be filtering options like priority etc. =====*/}
                <div className={`${styles.timeline__console}`}>
                    <ul className={`${styles.timeline__consoleItems}`}>
                        <li className={`${styles.timeline__consoleItem}`}>
                            <button className={`${styles.timeline__consoleBtn}`} onClick={scrollLeft}>
                                <Icons.ChevronLeftIcon />
                            </button>
                        </li>
                        <li className={`${styles.timeline__consoleItems}`}>
                            <button className={`${styles.timeline__consoleBtn}`} onClick={scrollRight}>
                                <Icons.ChevronRightIcon />
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default Timeline;
